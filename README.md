![Screenshot](git.png)

# "Git" el software de control de versiones!.

Git fue lanzado el 7 de abril del 2005, diseñado por Linus Torvalds, pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora y coordinar el trabajo que varias personas realizan sobre archivos compartidos. 

Ampliamente utilizado para el desarrollo de programas, script, website entre otros. Entre sus caracteristicas destacan:

* Software libre
* No depende de un repositorio central
* Se puede llevar un historial completo de versiones
* Tiene un sistemas de trabajo en ramas o branch.
* Puede rastrear cambios, volver a etapas anteriores entre otros.

Ahora manos a la obra! comenzamos!

Prerequisito: Debe contar con una cuenta en gitlab antes de iniciar este curso.

Nota: Solo se utilizara el usuario root para instalar paquetes, y un usuario nominal para trabajar con git.

## 1. Instalando git

Puede validar si git esta instalado y tambien su versión:

```
$ git --version
```

Si el comando no existe, puede instalarlo ejecutando los siguiente comandos:

* Cambie a usuario root

```
$ sudo su -
```

* Instale git

```
$ apt-get update
$ apt-get install git
$ exit
```

Ejecute nuevamente el comando "git --version" para validar la correcta instalación.

## 2. Configurando tu identidad

Lo primero a realizar es configurar su nombre de usuario y su email, esto es importante ya que cada modificación de archivos incluira su información para el rastreo:

```
$ git config --global user.name "Juan Perez"
$ git config --global user.email juan_perez@gmail.com
```

Estos comandos generan un archivo en el HOME del usuario. El archivo de llama ".gitconfig". La opción --global se aplicará para todos los repositorios que quiera trabajar. 

Lo siguiente es iniciar un repositorio como git, cree un directorio y ejecute:

```
$ mkdir ~/projecto; cd ~/projecto
$ git init
```
Este comando crea un subdirectorio llamado .git que contiene todos los archivos necesario para este repositorio. 

Opcional: Si quiere utilizar otra identidad para trabajar en otros proyectos, primero realice un "git init" para iniciar su directorio como "git" utilice "git config" sin la opción "--global", esto generá un registro en el directorio:

```
$ cd ~
$ mkdir projecto_2; cd projecto_2
$ git init
$ git config user.name "Juan Perez"
$ git config user.email juan_perez@gmail.com
```

El git init genera un directorio oculto llamado ".git" y en el archivo "config" registra la identidad:

```
$ cd .git
$ cat config
```

la salida similar a esto:

```
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[user]
	name = Juan Peres
	email = juan_perez@gmail.com
```


## 3. Controlando versiones


Desde este momento ya puede comenzar a controlar versiones de archivos existente o nuevos. Como ejemplo, cree unos archivos de prueba:

```
$ cd ~/projecto
$ echo "hola mundo" > file1.txt
$ echo "git es genial" > file2.txt
```

utilice el comando ``` git add ``` para especificar que archivos quiere controlar y ``` git commit ``` para confirmar los cambios:

```
$ git add file1.txt
$ git add file2.txt
$ git commit -m "primer commit"
```

Listo, ya estan versionados estos archivos. Mas adelante se explicará en detalle los comandos "git add" y "git commit".

## 4. Registro de commit

Todos los commit quedan registrados con el comando:

```
$ git log
```

La salida del comando:

```
commit 5919d9606b2daba25d49402e73a4020e354f348a (HEAD -> master, origin/master, origin/HEAD)
Author: Juan Perez <juan_perez@gmail.com>
Date:   Wed Jun 3 18:52:00 2020 -0400

    se agrega mas contenido: git init, git add y git push

commit 85dcf5a1a71a41e96dc642bb2d2bd9940f651aa9
Author: Juan Perez <juan_perez@gmail.com>
Date:   Wed Jun 3 18:26:26 2020 -0400

    Se agrega contenido de instalación y conf de git

```

## 5. Excluir archivos en el repositorio

Si requiere que algunos archivos no sean candidatos a versionar o enviar a repositorios remotos, por ejemplo archivos que contengan información privada como claves de usuario, variables de entorno entre otras posibilidades. Puede crear un archivo llamado ".gitignore" para que git ignore estos archivos como candidatos a pasar a la zona intermedia o stage.

```
$ touch .gitignore
```

Dentro del .gitignore puede agregar nombres de archivos, directorios, patrones de archivo. Por ejemplo:

* Ignore el archivo env.sh, agrege ```env.sh```
* Ignore archivos que en su nombre terminen con ~ , agregue ```*~```
* Ignore archivos que en su nombre termine con .conf, agregue ```*.conf```

Para que el archivo .gitignore funcione, debe confirmalo (commit) y si trabaja con repositorio remoto, debe enviar el ".gitignore":

```
$ git add .gitignore
$ git commit -m "added .gitignore"
```

Ahora los archivos que coincidan con los parametros de .gitignore no seran rastreados.
  
## 6. Flujo de trabajo

Git funciona con 3 arboles de trabajo:

* Directorio de trabajo = Contiene los archivos.
* Index (stage) = Zona intermedia, los archivos agregados con ```git add``` se encuentran en esta zona.
* Head = Cambios confirmados, generalmente a traves de ```git commit```.

La siguiente imagen simplifica la compresión del arbol de trabajo:

![Screenshot](git_stage.jpg)

Puede validar los estados del arbol de trabajo utilizando ```git status```.

## 7. Publicando en repositorios remotos


Ya contamos con un directorio con archivos versionados, ahora enviaremos todos estos archivo a un repositorio remoto en gitlab que previamente creo. 

![Screenshot](gitlab_remote.png) 

De acuerdo a la imagen, dentro de su proyecto en gitlab en la sección details seleccione "clone" y luego "Clone with HTTPS", copie el contenido ejecute "git remote add origin [pegue el contenido]" para agregar este repositorio remoto a ".git/config" .Por ejemplo:

```
git remote add origin https://gitlab.com/fregular/curso_git.git
```

Listo, ahora podemos publicar la rama por defecto (master) en su projecto de gitlab:

```
git push -u origin master
```

Lo anterior publicará sus commits en el repositorio gitlab. De ahora en adelante, si quiere publicar nuevos cambios, solo debe confirmar los cambios y ejecutar ```git push```

## 8. Volviendo al commit anterior

Puede que en algun momento quiera volver a un commit que anteriormente realizado. Para ello utilize ```git log``` y copie el "hash" del commit, y por ultimo ejecute ```git checkout [id_hash]```

Como ejemplo, crearemos el archivo bye.txt, lo agregamos, confirmamos y publicamos al repositorio remoto. Luego volveremos al commit anterior y veremos que sucede:

* Crear archivo

```
$ echo "blabla" > bye.txt
```

* Publimos:

```
$ git add bye.txt
$ git commit -m "add bye.txt in project"
$ git push
```

Listo, ahora para volver al commit anterior, revisamos el "git log"

![Screenshot](git_log.jpg)

Seleccionamos el penultimo hash del commit y ejecutar:


```
$ git revert --no-commit ffbdafe14929d284710abd16754305196d24b7a0..HEAD
```

Si validar el estado:
```
$ git status
On branch master
Your branch is up to date with 'origin/master'.

You are currently reverting commit 24243e1.
  (all conflicts fixed: run "git revert --continue")
  (use "git revert --abort" to cancel the revert operation)

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	deleted:    bye.txt
```

El archivo bye.txt sera marcado para borrar. Aplique finalmente un "git commit" para confirmar y "git push" para publicarlo.

## 8. Actualizando al commit mas nuevo

Si trabaja colaborativamente, puede que sus compañeros realicen cambios en el projectos y publiquen los cambios con ```git push```. Para no perder ningun cambio y actualizar al commit mas nuevo, ejecute:

```
$ git pull
```

## 9. Ramas (branch)

Las ramas son utilizadas para desarrollar funcionalidades aisladas unas de otras. La rama master es la rama "por defecto" cuando creas un repositorio. Crea nuevas ramas durante el desarrollo y fusiónalas(merge) a la rama principal (master) cuando termines. 

La siguiente imagen facilita la comprensión:

![Screenshot](git_branch_merge.png)

Para crear una rama nueva con la copia de la rama Master, ejecute ```git checkout -b ['nombre de la rama']```. Ejemplo:

```
$ git checkout -b 'release/test_branch'
```

Puede validar su rama actual con el comando ```git branch``` o tambien ```git status```. Si quiere visualizar las ramas del repositorio remoto, ejecute  ```git branch -r```.

Salida de de git branch:

```
$ git branch
  master
* release/test_branch
```

## 10. Fusion los cambios 

Git permite fusionar dos o mas ramas. Es util realizar un función al momento de crear una rama para trabajar en una nueva funcionalidad para luego incorporar esos cambios a la rama Master.

Para efectos de ejemplo, realice lo siguiente:

* Verifique que se encuentra en la rama "release/test_branch"

```
$ git branch
```

* cree un archivo de prueba:

```
$ echo "example of merge" > file_merge.txt
```

* Agregue, confirme y publique el cambio:

```
$ git add file_merge.txt
$ git commit -am "new file to test merge"
$ git push -u origin release/test_branch
```

* Cambie a la rama Master (valide que no tenga ningun archivo en stage):

``` 
$ git checkout master
```

* Fusione los cambios de la rama "release/test_branch" en master:

```
$ git merge "release/test_branch"
```

* Valide que los cambios fueron aplicados en Master y publique si es necesario.

```
git push
```

## 11. Deshacer un archivo preparado (stage) (agregado con ```git add```)

Git permite corregir archivos que por accidente agregamos con ```git add``` y que se encuentran en stage.

ejemplo:

* modifique un archivo ya confirmado:

```
$ touch conf.txt
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   conf.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

* Agregamos el archivo al stage area:

```
$ git add conf.txt

$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   conf.txt


```

* ```git status``` nos dice que podemos retirar el archivo "conf.txt" del stage area con el comando ```git reset HEAD <file>...```

```
$ git reset HEAD conf.txt
Unstaged changes after reset:
M	conf.txt

$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   conf.txt

no changes added to commit (use "git add" and/or "git commit -a")

```

El archivo ya no se encuentra rastreado.

## 12. Deshacer un cambio de un archivo

Si realizamos un cambio en un archivo, pero no quedamos conforme con el cambio y queremos deshacer los cambios y dejarlo al estado del ultimo commit:

* Modifique el archivo file1.txt

```
$ echo "que les vaya bien" >> file1.txt

```

* ejecute ```git status```

```
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   file1.txt

no changes added to commit (use "git add" and/or "git commit -a")
```
* La salida de git status nos aconseja ejecutar el ```git checkout -- <file>...``` para descartar los cambios. Ejecute:

```
$ git checkout -- file1.txt
```

Listo, tenemos el archivo "file1.txt" con los cambios del ultimo commit. Valide con un "cat file1.txt".


## 13. Etiquetas

Git tiene la posibilidad de etiquetar puntos específicos del historial como importantes. Esta funcionalidad se usa típicamente para marcar versiones de lanzamiento (v1.0, por ejemplo). 

* Para crear una etiqueta a un commit ya realizado:

```
$ git tag -a 1.0 -m "firts tag"
```

* Para validar que el tag ha sido creado:

```
$ git tag
1.0
```

* Validar que el commit este asociado al tag:

```
$ git show
commit dec021cc8c1a1c82449c25d9c12c65585a2b1f19 (HEAD -> master, tag: 1.0)
Author: Ubuntu <bt@brbktdevopstest.blacktourmaline.corp>
Date:   Thu Jun 4 21:48:28 2020 +0000

    modify conf.txt

diff --git a/conf.txt b/conf.txt
index b72e7d4..bdfebcb 100644
--- a/conf.txt
+++ b/conf.txt
@@ -1 +1,2 @@
 bla bla
+asd
```

Los tags tambien pueden ser publicados en repositorios remotos. Ejecute:

```
$ git push origin 1.0
```

Listo.

## 14. Eliminar un archivo ya confirmado

Si tenemos un archivo ya confirmado en su repositorio y quiere eliminarlo, puede utilizar el comando "git rm [file]" para eliminarlo y el archivo pasa directo al staging area.

* Borramos el archivo file1.txt

```
$ git rm file1.txt 
rm 'file1.txt'
```

* Revise el estado

```
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	deleted:    file1.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	conf.txt
```

* confirme para que el archivo sea eliminado

```
$ git commit -m "delete file1.txt"
[master 3ec254d] delete file1.txt
 1 file changed, 1 deletion(-)
 delete mode 100644 file1.txt
```

Listo, puede publicarlo si desea con "git push". Cual es la diferencia de eliminar manualmente a utilizar "git rm"?, al eliminarlo con "git rm" el cambio pasa directamente al staging, pero al eliminarlo con "rm" el archivo no pasa al staging area y debe agregarse manualmente con un "git add" y confirmarlo para eliminarlo.

## 15. Git diff

Este comando muy util para revisar las diferencias entre archivos que estan confirmados vs archivo que estamos modificando. Por ejemplo

* Modifique el archivo file2.txt

```
$ echo "otra linea" >> file2.txt
```

* Revise el estado

```
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   file2.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	conf.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

El archivo aun se encuentra el area de trabajo, ejecute el comando git diff :

```
$ git diff
diff --git a/file2.txt b/file2.txt
index 7bfd20d..73cb2d1 100644
--- a/file2.txt
+++ b/file2.txt
@@ -1 +1,2 @@
 git es genial
+otra linea
```
La salida "--- a/file2.txt" corresponde al archivo en el repositorio, y el "+++ b/file2.txt" el archivo modificado. El signo "+" indica la diferencia entre el archivo file2.txt del repositorio (HEAD) vs la modificación del archivo file2.txt. El comando "git diff" compará todos los archivos, pero tambien puede pasar un archivo especifico.

Si ejecutamos un "git add file2.txt" ya no podremos visualizar la diferencia.

```
$ git add file2.txt 
$ git diff
```

No hay salida del comando git diff. Para observar la diferencia utilice el comando "git diff --cached"
 
```
$ git diff --cached 
diff --git a/file2.txt b/file2.txt
index 7bfd20d..73cb2d1 100644
--- a/file2.txt
+++ b/file2.txt
@@ -1 +1,2 @@
 git es genial
+otra linea
```

Al invocar git diff con la opción --cached, el comando comparará los cambios preparados con el repositorio local. La opción --cached equivale a --staged. Publique el cambio con "git commit" y "git push".

Tambien es posible realizar un "git diff" entre ramas:

```
$ git diff master release/test_branch
diff --git a/file1.txt b/file1.txt
new file mode 100644
index 0000000..24db42b
--- /dev/null
+++ b/file1.txt
@@ -0,0 +1 @@
+hola mundo
diff --git a/file2.txt b/file2.txt
index 73cb2d1..7bfd20d 100644
--- a/file2.txt
+++ b/file2.txt
@@ -1,2 +1 @@
 git es genial
-otra linea
```

La salida indica que en Master el archivo file1.txt no existe (--- /dev/null), y que el archivo file2.txt de release/test_branch no contiene la linea (-otra linea).

## 16. Clon de projecto y CI/CD en gitlab

CI/CD es un método para distribuir aplicaciones de forma periódica, mediante el uso de la automatización en las etapas del desarrollo de las aplicaciones. Los principales conceptos que se atribuyen a un CI/CD son la integración continua (CI), la distribución continua (CD) y la implementación continua (el otro CI). Esto da como resultado la resolución de los problemas que pueda generar la integración de nuevo código entre tu equipo de desarrolladores.

Para demostrar un poco como funciona el CI/CD de gitlab, favor realizar este ejercicio:

* Clone el projecto del curso con el siguiente comandos:

```
$ cd ~
$ git clone https://gitlab.com/gole-group/devops-base-01/git.git
$ cd git
```

* Cree un rama con el nombre de su usuario asignado para el curso:

```
$ git checkout -b 'user_devopsxx'
```

* Visualice el archivo .gitlab-ci.yml para validar si la configuración esta correcta.


* Confirme los cambios y publiquelos

```
$ git add .gitlab-ci.yml
$ git commit -am "actualizando .gitlab-ci.yaml"
$ git push -u origin user_devopsxx
```

* ingrese a gitlab al proyecto del curso, y ingrese a CI/CD - jobs. Visualice si la ejecución del runner finalizo sin problemas. Si falla, se debe validar en que punto fallo y corregir el archivo .gitlab-ci.yml y volver a confirmar y enviarlo al repositorio del curso.



